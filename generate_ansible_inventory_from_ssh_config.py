#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This script assume each "Host", "Hostname", "Port", "User" in separated line, and valid ssh config file.


import sys
import argparse


def print_host( host, hostname, port, user):
   print(f"{host}:")
   if hostname:
      print(f"  ansible_host: {hostname}")
   if port:
      print(f"  ansible_port: {port}")
   if user:
      print(f"  ansible_user: {user}")


if __name__ == "__main__":
   parser = argparse.ArgumentParser(
      description="Umwandlung von SSH config zu Ansible YAML."
   )
   parser.add_argument(
      "--file",
      action="store",
      dest="file_path",
      help="path to SSH config",
      default=".ssh/config",
   )
   try:
      args = parser.parse_args()
   except:
      parser.print_help()
      sys.exit(1)

   with open(args.file_path, newline="", encoding="utf-8") as file:
      host = ""
      hostname = ""
      port = ""
      user = ""
      for line in file:
         data = line.strip().split(maxsplit=1)
         if len(data)==0:
            continue
         if data[0].lower()=="host":
            # if host is set, a new host begins here
            # so print out last host
            if host:
               print_host( host, hostname, port, user)
            host=data[1]
            hostname=""
            port=""
            user=""
         if data[0].lower()=="hostname":
            hostname=data[1]
         if data[0].lower()=="port":
            port=data[1]
         if data[0].lower()=="user":
            user=data[1]
      if host:
         print_host( host, hostname, port, user)        
