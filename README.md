# mk_ansible

## Ein Ansible Playbook, um einen neuen SSH-Key auszurollen und alte Schlüssel zu entfernen


## Inventory

Falls noch kein Inventory mit den eigenen Hosts existiert, kann man sich mit:

```
python generate_ansible_inventory_from_ssh_config.py --file=/home/<id>/.ssh/config
```

zumindest eine Hostliste aus der SSH config erzeugen.

## Schlüssel anpassen

Im inventory.yaml die Schlüssel verlinken, dabei die Dateinamen für die privaten Schlüssel verwenden

```
ansible-playbook -i inventory.yaml replace_keys.yaml --limit <host>
```
